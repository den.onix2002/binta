import * as flsFunctions from "./modules/functions.js";
import $ from 'jquery';
// export for others scripts to use
flsFunctions.isWebp();
'use strict';
const config = {
    'sum_users_multiplier': '700',
    'currency': 'EUR',
    'baseUrl': 'https://console.binta.ru',
    'paramsQuery': {'domain_zone': 'ru'},
}


$(document).ready(function () {
    $("#header").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});

$('.binta_questions__cards__card').on('click', (e) => {
    $(e.currentTarget).children('.binta_questions__cards__card__text').toggle('fast')
    if ($(e.currentTarget).find('.plus_span').css('transform') !== 'none') {
        $(e.currentTarget).find('.plus_span').css('transform', 'none')
        $(e.currentTarget).find('.minus_span').css({
            'left': '25%',
            'right': '25%'
        })
    } else {
        $(e.currentTarget).find('.minus_span').css({
            'left': '50%',
            'right': '50%'
        })
        $(e.currentTarget).find('.plus_span').css({
            'transform': 'rotate(90deg)',
        })
    }

})

let usersCounter = parseInt($('#users_counter').text());
let basketTotalAmountNumber = parseInt($('#basket_total_amount__number').text());
let basketTotalAmountBonusNumber = parseInt($('#basket_total_amount__bonus__number').text());

let sumUsersMultiplier = config.sum_users_multiplier

var url = `${config.baseUrl}/instance/modules/info?` + $.param(config.paramsQuery)


$('.second-button').on('click', function () {
    $('.animated-icon2').toggleClass('open');
    let isMenuOpen = $('.animated-icon2').hasClass('open')
    if (isMenuOpen) {
        let size = window.innerWidth <= 450 ? '100%' : '250px';
        $('#binta_sidenav').css({'width': size});
        $('#binta_body').css({'touch-action': 'none'});
        $('.navbar-toggler').css({'position': 'fixed'});
    } else {
        $('#binta_sidenav').css({'width': '0'});
        $('#binta_body').css({'touch-action': 'auto'});
        $('.navbar-toggler').css({'position': 'absolute'});
    }
});

$('.sidenav__uncorn_link').on('click',function (){
    $('.navbar-toggler').css({'position': 'absolute'});
})


function appendServicesCards(category, currency) {
    $("#srivec_cards_row").append(function () {
        return `<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12"  data-bs-toggle="popover"

                    data-bs-content="${category.category_desc ? category.category_desc : 'Empty'}">
                    <article id="${category.category_id}" class="service_card">
                        <div class="row">
                            <div class="col-7 d-flex align-items-center">
                                <img class="service_card__icon" src="./img/${category.category_name}.png" alt="">
<!--                                <p class="service_card__header" title="${category.category_name}">${category.category_name}</p>-->
                                <p class="technical_name">${category.category_name}</p>
                            </div>
                            <div class="col-5 d-flex align-items-center justify-content-end">
                                <div class="service_card__cost d-flex align-items-end">
                                    <p class="service_card__cost__number">${category.category_price}</p>
                                    <p class="service_card__cost__currency">${currency}</p>
                                </div>
                                    <div class="custom_round_checkbox_container d-flex">
                                    <input type="checkbox" id="custom_round_checkbox_talking${category.category_id}"/>
                                    <label for="custom_round_checkbox_talking${category.category_id}"></label>
                                    </div>
                            </div>
                    </div>
            </article>
        </div>`
    });
}

fetch(url, {
    method: 'GET',
    mode: 'cors',
}).then(response => {
    return response.json()
}).then(data => {
    let module_categories = data.module_categories
    let currency = data.currency_label
    config.currency = currency
    $.each(module_categories, function (index, value) {
        appendServicesCards(value, currency)
    });
    $('.service_cards_preloader').remove()
    $('.service_card__cost__currency').text(currency);
    $('.basket_total_amount__currency').text(currency);
    // sumUsersMultiplier.currency = currency;
    setOnclickServiceCard();
    $(function () {
        window.$('[data-bs-toggle="popover"]').popover(
            {
                trigger: 'hover',
                placement: 'top',
            }
        )
    })
})

function removeServicesCardsUsers(usersCounter, sum) {
    if (usersCounter === 0) {
        let basketElements = $(".basket_items_container").find('div.basket_item')
        if (basketElements.length < 4) {
            $('#basket_item__users_counter').remove()
            $(".basket_items_container").append(function () {
                return '<div class="basket_item d-flex flex-column justify-content-center emty_basket_element"></div>'
            });
        } else {
            $('#basket_item__users_counter').remove()
        }
    }
    let basketTotalAmountNumber = parseInt($('#basket_total_amount__number').text() - sumUsersMultiplier)
    $('#basket_item__users_count').text(usersCounter);
    $('#basket_total_amount__number').text(basketTotalAmountNumber);
    $('#basket_item__currency_number__users_counter').text(sum);
}

$('#btn-create-demo').click(function () {

})

$('#users_minus_btn').click(function () {
    if (usersCounter === 0) {
        $('#users_sum_counter').text(0);
        return
    }
    ;
    usersCounter--;
    let sum = usersCounter * sumUsersMultiplier
    removeServicesCardsUsers(usersCounter, sum)
    $('#users_counter').text(usersCounter);
    $('#users_sum_counter').text(sum);
    setBusketHeight()
    setEnableButton()
})

function appendServicesCardsUsers(users_count, sum, currency) {
    let basketTotalAmountNumber = parseInt($('#basket_total_amount__number').text()) + parseInt(sumUsersMultiplier)
    if ($('#basket_item__users_counter').length === 0) {
        let template = `<div id="basket_item__users_counter" class="basket_item d-flex flex-column justify-content-center">` +
            '<div class="row">' +
            '<div class="col-8 d-flex align-items-center">' +
            `<p id="basket_item__users_count" class="basket_item__users_count pr-2">${users_count}</p>` +
            `<h6 class="basket_item__name">Пользователей</h6>` +
            '</div>' +
            '<div class="col-4 d-flex align-items-end justify-content-end">' +
            `<p id="basket_item__currency_number__users_counter" class="basket_item__currency_number">${sum}</p>` +
            `<p class="basket_item__currency">${currency}</p>` +
            '</div>' +
            '</div>' +
            '</div>'
        checkEmptyBasketElements(template)
    } else {
        $('#basket_item__currency_number__users_counter').text(sum)
    }
    $('#basket_item__users_count').text(users_count);
    $('#basket_total_amount__number').text(basketTotalAmountNumber);

}

function checkPressPlus(emptyElements) {
    if (emptyElements.length) {
        emptyElements.first().replaceWith(template)
    } else {
        $(".basket_items_container").prepend(function () {
            return template
        });
    }
}

function checkPressMinus() {

}

function checkEmptyBasketElements(template) {
    let emptyElements = $(".basket_items_container").find('div.emty_basket_element')
    if (emptyElements.length) {
        emptyElements.first().replaceWith(template);
    } else {
        $(".basket_items_container").prepend(function () {
            return template
        });
    }
}

$('#users_plus_btn').click(function () {
    usersCounter++;
    let usersSumCounter = usersCounter * sumUsersMultiplier
    appendServicesCardsUsers(usersCounter, usersSumCounter, config.currency)
    $('#users_counter').text(usersCounter);
    $('#users_sum_counter').text(usersSumCounter);
    setBusketHeight()
    setEnableButton()

})

$(window).on('load', function () {
    $("#preloader_container").fadeOut(1000, function () {
        $(this).remove();
    });
})

$('#binta_call_back_form__lead__modal').submit(function (event) {

    let settings = {
        "url": `${config.baseUrl}/instance/crm?&email=${$("#modal_demonstartion_input_email").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#modal_demonstartion_input_phone").val()}&name=${$("#modal_demonstartion_input_name").val()}&lead_type=request`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function () {
        window.$('#modal_lead_form_callback').modal('hide');
        window.$('#modal_information_manager_callback').modal('show');
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    });

    this.reset();

    event.preventDefault();
})

$('#binta_call_back_form__header').submit(function (event) {
    let settings = {
        "url": `${config.baseUrl}/instance/crm?&name=${$("#head_input_name").val()}&email=${$("#head_input_email").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#head_input_phone").val()}&company_name=${$("#head_input_company").val()}&domain=${$("#head_input_domain").val()}&lead_type=demo`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function (result) {
        window.$('#modal_information_manager_callback').modal('show');
        $('#modal-message-text').text(JSON.parse(result).message);
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    });

    this.reset();

    event.preventDefault();
})

$('#binta_call_back_form__header__mobile').submit(function (event) {
    let settings = {
        "url": `${config.baseUrl}/instance/crm?&name=${$("#head_input_name_mobile").val()}&email=${$("#modal_demonstartion_input_email").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#head_input_phone_mobile").val()}&company_name=${$("#head_input_company_mobile").val()}&domain=${$("#head_input_domain_mobile").val()}&lead_type=demo`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function (result) {
        window.$('#modal_information_manager_callback').modal('show');
        $('#modal-message-text').text(JSON.parse(result).message);
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    });

    this.reset();

    event.preventDefault();
})

$('#binta_call_back_form__today_mobile').submit(function (event) {
    let settings = {
        "url": `${config.baseUrl}/instance/crm?&name=${$("#head_demo_input_name_mobile").val()}&email=${$("#head_demo_input_email_mobile").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#head__demo_input_phone_mobile").val()}&company_name=${$("#head_demo_input_company_mobile").val()}&domain=${$("#head_demo_input_domain_mobile").val()}&lead_type=demo`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function (result) {
        window.$('#modal_information_manager_callback').modal('show');
        $('#modal-message-text').text(JSON.parse(result).message);
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    });

    this.reset();

    event.preventDefault();
})


$('#binta_call_back_form__today').submit(function (event) {
    let settings = {
        "url": `${config.baseUrl}/instance/crm?&name=${$("#head_input_name_start").val()}&email=${$("#head_input_email_start").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#head_input_phone_start").val()}&company_name=${$("#head_input_company_start").val()}&domain=${$("#head_input_domain_start").val()}&lead_type=demo`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function (result) {
        window.$('#modal_information_manager_callback').modal('show');
        $('#modal-message-text').text(JSON.parse(result).message);
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    });

    this.reset();

    event.preventDefault();
})

$('#binta_call_back_form__lead').submit(function (event) {
    let settings = {
        "url": `${config.baseUrl}/instance/crm?&email=${$("#demonstartion_input_email").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#demonstartion_input_phone").val()}&name=${$("#demonstartion_input_name").val()}&lead_type=request`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function (result) {
        window.$('#modal_information_manager_callback').modal('show');
        $('#modal-message-text').text(JSON.parse(result).message);
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    });

    this.reset();

    event.preventDefault();
})

$('#binta_call_back_form__footer').submit(function (event) {
    let settings = {
        "url": `${config.baseUrl}/instance/crm?&name=${$("#start_now_input_name").val()}&email=${$("#start_now_input_email").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#start_now_input_phone").val()}&company_name=${$("#start_now_input_company").val()}&domain=${$("#start_now_input_domain").val()}&lead_type=demo`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function (result) {
        window.$('#modal_information_manager_callback').modal('show');
        $('#modal-message-text').text(JSON.parse(result).message);
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    });

    this.reset();

    event.preventDefault();
})

$('#binta_call_back_form__modal').submit(function (e) {
    let installed_modules_array = []
    let basketItemElements = $(".basket_items_container").find('p.technical_name')
    $.each(basketItemElements, function (index, element) {
        let technical_name = element.innerText
        if (technical_name) installed_modules_array.push(technical_name)
    });
    let valid_list_technical_names = installed_modules_array.join(',')
    // $(this).find('input.modules_to_install').val(valid_list_techical_names)
    let settings = {
        "url": `${config.baseUrl}/instance/crm?&name=${$("#modal_head_input_name").val()}&email=${$("#modal_head_input_email").val()}&request_domain_zone=${config.paramsQuery.domain_zone}&phone=${$("#modal_head_input_phone").val()}&company_name=${$("#modal_head_input_company").val()}&domain=${$("#modal_head_input_domain").val()}&lead_type=demo&modules_to_install=${valid_list_technical_names}`,
        "method": "POST",
        "timeout": 0,
    };

    $.ajax(settings).done(function (result) {
        window.$('#modal_information_manager_callback').modal('show');
        $('#modal-message-text').text(JSON.parse(result).message);
    }).fail(function (xhr, status, error) {
        console.log(`XHR: ${xhr};\n STATUS: ${status};\n ERROR ${error}`)
    })

    this.reset();

    window.$('#modal_form_callback').modal('hide');



    e.preventDefault();

});

document.querySelectorAll('a[href*=\\#]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        if ($(this).attr('class') === 'plus_minus' || $(this).attr('class') === "busket_start_work_btn btn btn-info d-flex justify-content-center") {
            return
        } else if ($(this).attr('class') === 'sidenav__uncorn_link') {
            $('#binta_sidenav').css({'width': '0'})
            $('.animated-icon2').toggleClass('open');
            $('#binta_body').css({'touch-action': 'auto'});
        }
        ;
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    let scrollElement = document.getElementsByClassName('to_top_arrow_btn')[0]
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        scrollElement.style.display = "flex";
    } else {
        scrollElement.style.display = "none";
    }
}

$('#busket_start_work_btn').on('click', function () {
    window.$('#modal_form_callback').modal('show');
});

$("p[data-dismiss=modal]").on('click', function () {
    window.$('#modal_form_callback').modal('hide');
    window.$('#modal_lead_form_callback').modal('hide');
});

$('.additional__service_card__btn').on('click', function () {
    window.$('#modal_lead_form_callback').modal('show');
});
$('#modal_start_work_btn').on('click', function () {
    window.$('#modal_information_manager_callback').modal('hide');
});


function setBusketHeight() {
    let basketItemElements = $(".basket_items_container").find('div.basket_item')
    if (basketItemElements.length < 8) {
        $('.basket_items_container').css({'height': 'auto', 'overflow-y': 'unset'})
    } else if (basketItemElements.length === 8) {
        let actual_height = $('.basket_items_container').height()
        $('.basket_items_container').css({'height': actual_height, 'overflow-y': 'auto'})
    }
}

function setEnableButton() {
    let basketItemElements = $(".basket_items_container").find('div.basket_item[id]')
    if (basketItemElements.length === 0) {
        $('#busket_start_work_btn').attr("disabled", true);
    } else {
        $('#busket_start_work_btn').removeAttr('disabled')
    }
}

function setOnclickServiceCard() {
    $(".service_card").click(function (e) {

        if (!$(e.target).is('input:checkbox')) {
            var $checkbox = $(this).find('input:checkbox');
            $checkbox.prop('checked', !$checkbox.prop('checked'));
            let itemImage = $('<div>').append($(this).find('img.service_card__icon').clone()).html();
            let itemId = 'basket_item__' + $(this).attr('id')
            let itemName = $(this).find('p.service_card__header').text()
            let technicalName = $(this).find('p.technical_name').text()
            let itemCurrencyNumber = $(this).find('p.service_card__cost__number').text()
            let itemCurrency = $(this).find('p.service_card__cost__currency').text()
            let tempate = `<div id="${itemId}" class="basket_item d-flex flex-column justify-content-center">` +
                '<div class="row">' +
                '<div class="col-7 d-flex align-items-center">' +
                `${itemImage}` +
                `<h6 class="basket_item__name" title="${itemName}">${itemName}</h6>` +
                `<p class="technical_name">${technicalName}</p>` +
                '</div>' +
                '<div class="col-5 d-flex align-items-end justify-content-end">' +
                `<p class="basket_item__currency_number">${itemCurrencyNumber}</p>` +
                `<p class="basket_item__currency">${itemCurrency}</p>` +
                '</div>' +
                '</div>' +
                '</div>'

            if ($checkbox.prop('checked')) {
                $(this).css({'box-shadow': 'inset 0px 0px 0px 2px rgba(0, 179, 198,0.64)'})
                let emptyElements = $(".basket_items_container").find('div.emty_basket_element')
                let totalNumber = parseInt($('#basket_total_amount__number').text()) + parseInt(itemCurrencyNumber)
                $('#basket_total_amount__number').text(totalNumber);
                $('#basket_total_amount__bonus__number').text((totalNumber * 2) / 10);
                $('#basket_total_amount__bonus__small__number').text((totalNumber * 2) / 10);
                if (emptyElements.length) {
                    emptyElements.first().replaceWith(tempate)
                } else {
                    $(".basket_items_container").append(function () {
                        return tempate
                    });
                }
            } else {
                $(this).css({'box-shadow': '0px 4.45455px 16.5455px rgb(0 82 96 / 13%)'})
                let totalNumber = parseInt($('#basket_total_amount__number').text()) - parseInt(itemCurrencyNumber)
                $('#basket_total_amount__number').text(totalNumber);
                $('#basket_total_amount__bonus__number').text((totalNumber * 2) / 10);
                $('#basket_total_amount__bonus__small__number').text((totalNumber * 2) / 10);
                $(`#${itemId}`).remove()
                let emptyElements = $(".basket_items_container").find('div.basket_item')
                if (emptyElements.length < 3) {
                    $(".basket_items_container").append(function () {
                        return '<div class="basket_item d-flex flex-column justify-content-center emty_basket_element"></div>'
                    });
                }
            }
            setBusketHeight();
            setEnableButton();
        }
        e.preventDefault();
    });
}


